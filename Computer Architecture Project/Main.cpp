#include <iostream>
#include <string>
#include <thread>
#include <chrono>

using namespace std;

string getBooleanAlgebrea(int x){
	switch(x){
	case 1:
		return "T1\n\t(a)  AvB=BvA\n\t(b)  A^B=B^A ";
		break;
	case 2:
		return "T2\n\t(a)  (A^B)^C=A^(B^C)\n\t(b)  (AvB)vC=Av(BvC) ";
		break;
	case 3:
		return "T3\n\t(a)  Av(B^C)=AvB^AvC\n\t(b)  A^(BvC)=(A^B)v(A^C) ";
		break;
	case 4:
		return "T4\n\t(a)  A^A=A\n\t(b)  AvA=A ";
		break;
	case 5:
		return "T5\n\t(a)  (AvB)^(Av~B)=A\n\t(b)  (A^B)^(A^~B)=A ";
		break;
	case 6:
		return "T6\n\t(a)  A^AvB=A\n\t(b)  Av(A^B)=A ";
		break;
	case 7:
		return "T7\n\t(a)  0^A=A\n\t(b)  0vA=0 ";
		break;
	case 8:
		return "T8\n\t(a)  1^A=1\n\t(b)  1vA=A ";
		break;
	case 9:
		return "T9\n\t(a) ~A^A=1\n\t(b)  ~AvA=0 ";
		break;
	case 10:
		return "T10\n\t(a)  A^~AvB=A^B\n\t(b)  Av(~A^B)=AvB ";
		break;
	case 11:
		return "T11\n\t(a)  ~(A^B)=~Av~B\n\t(b)  ~(AvB)=~A^~B ";
		break;
	
	
	}

}
int BinaryToDec(string Number){
	int temp = 0;
	for (int x = 0; x < Number.length(); x++){
		if (Number[(Number.length()-x)-1] != '0')
		temp += pow(2,x);	
	}
	return temp;
}
string DecToBinary(int Number){
	int counter = Number;
	int index = log((double)Number) / log((double)2);
	string temp(index+1,'0');
	temp[0] = '1';
	counter -= pow(2, index);
	while (counter != 0){
		index = log((double)counter) / log((double)2);
		temp[(temp.length()-(index+1))] = '1';
		counter -= pow(2, index);
	}
	return temp;
}
int HextoDec(string Number){
	int temp = 0;
	for (int x = 0; x < Number.length(); x++){
		switch (Number[x]){
		case '0':
			temp += 0 * pow(16, (Number.length() - x) - 1);
			break;
		case '1':
			temp += 1 * pow(16, (Number.length() - x) - 1);
			break;
		case '2':
			temp += 2 * pow(16, (Number.length() - x) - 1);
			break;
		case '3':
			temp += 3 * pow(16, (Number.length() - x) - 1);
			break;
		case '4':
			temp += 4 * pow(16, (Number.length() - x) - 1);
			break;
		case '5':
			temp += 5 * pow(16, (Number.length() - x) - 1);
			break;
		case '6':
			temp += 6 * pow(16, (Number.length() - x) - 1);
			break;
		case '7':
			temp += 7 * pow(16, (Number.length() - x) - 1);
			break;
		case '8':
			temp += 8 * pow(16, (Number.length() - x) - 1);
			break;
		case '9':
			temp += 9 * pow(16, (Number.length() - x) - 1);
			break;
		case 'a':
			temp += 10 * pow(16, (Number.length() - x) - 1);
			break;
		case 'b':
			temp += 11 * pow(16, (Number.length() - x) - 1);
			break;
		case 'c':
			temp += 12 * pow(16, (Number.length() - x) - 1);
			break;
		case 'd':
			temp += 13 * pow(16, (Number.length() - x) - 1);
			break;
		case 'e':
			temp += 14 * pow(16, (Number.length() - x) - 1);
			break;
		case 'f':
			temp += 15 * pow(16, (Number.length() - x) - 1);
			break;
		case 'A':
			temp += 10 * pow(16, (Number.length() - x) - 1);
			break;
		case 'B':
			temp += 11 * pow(16, (Number.length() - x) - 1);
			break;
		case 'C':
			temp += 12 * pow(16, (Number.length() - x) - 1);
			break;
		case 'D':
			temp += 13 * pow(16, (Number.length() - x) - 1);
			break;
		case 'E':
			temp += 14 * pow(16, (Number.length() - x) - 1);
			break;
		case 'F':
			temp += 15 * pow(16, (Number.length() - x) - 1);
			break;
		}

	}

	return temp;
}
string DecToHex(int Number){
	int counter = Number;
	int index = log((double)Number) / log((double)16);
	string Hex (index+1,'0');
	int t1, t2;
	while(counter != 0){
		index = log((double)counter) / log((double)16);
		for (int x = 0; x < 17; x++){
			t2 = x;
			if (counter - (t2*pow(16, index)) >= 0){
			t1 = t2;
			}
		}
		counter -= (t1*pow(16, index));
		switch (t1){
		case 0:
			Hex[(Hex.length()-index)-1]= '0';
			break;
		case 1:
			Hex[(Hex.length()-index)-1]= '1';
			break;
		case 2:
			Hex[(Hex.length()-index)-1]= '2';
			break;
		case 3:
			Hex[(Hex.length()-index)-1]= '3';
			break;
		case 4:
			Hex[(Hex.length()-index)-1]= '4';
			break;
		case 5:
			Hex[(Hex.length()-index)-1]= '5';
			break;
		case 6:
			Hex[(Hex.length()-index)-1]= '6';
			break;
		case 7:
			Hex[(Hex.length()-index)-1]= '7';
			break;
		case 8:
			Hex[(Hex.length()-index)-1]= '8';
			break;
		case 9:
			Hex[(Hex.length()-index)-1]= '9';
			break;
		case 10:
			Hex[(Hex.length()-index)-1]= 'A';
			break;
		case 11:
			Hex[(Hex.length()-index)-1]= 'B';
			break;
		case 12:
			Hex[(Hex.length()-index)-1]= 'C';
			break;
		case 13:
			Hex[(Hex.length()-index)-1]= 'D';
			break;
		case 14:
			Hex[(Hex.length()-index)-1]= 'E';
			break;
		case 15:
			Hex[(Hex.length()-index)-1]= 'F';
			break;
		}
	}
	return Hex;
	
}
string ToOnes(string x){
	for (int index = 0; index < x.length();index++){
		if (x[index] == '0')
			x[index] = '1';
		else
			x[index] = '0';
	}
	return "1"+x;
}
string ToTwos(string x){
	bool addFlag = 1;
	for (int index = x.length()-1; index != 0; index--){
		if (addFlag)
		if (x[index] == '0'){
			addFlag = 0;
			x[index] = '1';
		}
		else
			x[index] = '0';
	}
	return x;
}
int main(){
	int Bool = -1;
	int menu = -1;
	while (1 != 0){	
		
		while (!(menu >= 0 && menu <= 7)){
			cout << "============Computer Architecture Fianl Project==============" << endl;
			cout << "1)Decimal to Binary" << endl;
			cout << "2)Binary to Decimal" << endl;
			cout << "3)Decimal to Hex" << endl;
			cout << "4)Hex to Decimal" << endl;
			cout << "5)Ones complement conversion" << endl;
			cout << "6)Two's complement conversion" << endl;
			cout << "7)Boolean Algebra Laws" << endl;
			cout << "0)Exit" << endl;
			cout << ">> ";
			cin >> menu;
			cout << "Please Enter a Valid option" << endl;
		}
			std::system("cls");
		string buffer;
		switch (menu){
		case 1:
			cout << "Please enter a Decimal Number to convert to a Binary Number: ";
			cin >> buffer;
			cout << buffer << " as a Binary Number is: " << DecToBinary(stoi(buffer))<< endl;
			break;
		case 2:
			cout << "Please enter a Binary Number to convert to a Decimal Number: ";
			cin >> buffer;
			cout << buffer << " as a Decimal Number is: " << BinaryToDec(buffer) << endl;
			break;
		case 3:
			cout << "Please enter a Decimal Number to convert to a Hex Number: ";
			cin >> buffer;
			cout << buffer << " as a Hex Number is: " << DecToHex(stoi(buffer)) << endl;
			break;
		case 4:
			cout << "Please enter a Hex Number to convert to a Decimal Number: ";
			cin >> buffer;
			cout << buffer << " as a Decimal Number is: " << HextoDec(buffer) << endl;
			break;
		case 5:
			cout << "Please enter a Decimal Number to convert to a Ones complement Number: ";
			cin >> buffer;
			if (stoi(buffer) < 0){
				cout << buffer << " as a  Ones complement Number is: " << ToOnes(DecToBinary(abs(stoi(buffer)))) << endl;
				
			}
			else
				cout << buffer << " as a  Ones complement Number is: " <<DecToBinary(abs(stoi(buffer)))<< endl;
			break;
		case 6:
			cout << "Please enter a Decimal Number to convert to a Twos complement Number: ";
			cin >> buffer;
			if (stoi(buffer)<0)
				cout << buffer << " as a  Twos complement Number is: " << ToTwos(ToOnes(DecToBinary(abs(stoi(buffer))))) << endl;
			else
				cout << buffer << " as a  Twos complement Number is: " << ToTwos(DecToBinary(stoi(buffer))) << endl;
			
			break;
		case 7:
		
			while (!(Bool >= 0 && Bool <= 11)){
				cout << "===============Boolean Algrbral Laws===========" << endl;
				cout << "1)T1" << endl;
				cout << "2)T2" << endl;
				cout << "3)T3" << endl;
				cout << "4)T4" << endl;
				cout << "5)T5" << endl;
				cout << "6)T6" << endl;
				cout << "7)T7" << endl;
				cout << "8)T8" << endl;
				cout << "9)T9" << endl;
				cout << "10)T10" << endl;
				cout << "11)T11" << endl;
				cout << ">> ";
				cin >> Bool;
				cout << "Please Enter a Valid option" << endl;
			}
			std::system("cls");
			cout << getBooleanAlgebrea(Bool) << endl;
			break;
		case 0:
			std::system("pause");
			return 0;
			break;
		}
		std::system("pause");
		std::system("cls");
		menu = -1;

	}
std::system("pause");
	return 0;
}